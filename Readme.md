MODULE DOCKER
---

![Docker Icon for Docker Module](/docker.png "Containerisation with Docker")

**Commandes exécutées dans les cours**

<details>

<summary>DOCKER</summary>
<br>

- `docker pull <image>:<tag>`=Télécharge et lance une image <br>

- `docker container ls`=Affiche les conteneurs en cours d'exécution 
- `docker container ls -a`=Affiche tous les conteneurs 
- `docker image ls `=Affiche les images
- `docker container run <image>:<tag>`=Exécute un conteneur à partir de l'image. Télécharge l'image si pas présente
- `docker container run -d <image>:<tag>`=Exécute un conteneur en mode detach
- `docker container stop/start <ID/name>`=Démarre ou arrête un conteneur 
- `docker container run -p <host_port>:<container_port> <image>:<tag>`=Map un port de la machine hôte à celui du conteneur 
- `docker container run --name <container_name> <ID/name>`=Exécute un conteneur avec le nom défni
- `docker container run -e ENVIRONMENT_VARIABLE=value <ID/name>`=Exécute un conteneur avec des variables d'environnement
- `docker container run -v /host/dir:/container/dir <ID/name>`=Exécute un conteneur en mappant un volume hôte à celui du conteneur
- `docker container rm <ID/name>`=Supprime un conteneur
- `docker container rm $(docker container ls)`=Supprime tous les conteneurs actifs
- `docker container prune`=Supprime tous les conteneurs non actif
- `docker build -t <name> .`=Crée une image à partir du Dockerfile. "." indique l'emplacement du Dockerfile.
- `docker login`=Authentifie sur un dépôt
- `docker tag`=renomme une image 
- `docker push`=Pousse une image sur un dépôt
- `docker image rm <ID/name>`=Supprime une image
- `docker image rm $(docker image ls)`=Supprime toutes les images
- `docker logs <ID/name>`=Affiche les logs d'un conteneur 
- `docker inspect <ID/name>`=Affiche des informations système sur le composant
- `docker exec -it <ID/name> /bin/bash`=Accède au conteneur avec le bash comme interpréteur  
- `docker system prune`=Supprime Conteneur/Réseau/image etc 
</details>


<details>
<summary>DOCKER-COMPOSE</summary> 
<br>

- `docker-compose -f docker-compose.yaml up -d`=Exécute le Compose en detach mode <br>

- `docker-compose -f docker-compose.yaml down`=Stop tous les services crées par ce fichier
</details>






